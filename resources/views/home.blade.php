
<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>Hiru - Admin dashboard</title>
@include('layouts.link')

</head>

<body class="fixed-nav sticky-footer" id="page-top">
@include('layouts.sidebar')
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Profile</li>
        </ol>
        <div class="box_general padding_bottom">
            <div class="header_box version_2">
                <h2><i class="fa fa-user"></i>Profile details</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Your photo</label>
                        <div class="circle-plus">
                            <img class="card-img" src="/images/profile_images/{{$user['profile_image']}}">
                        </div>
                    </div>
                </div>


                <div class="col-md-8 add_top_30">
                    <div class="row">
                        <div class="col-md-6">

                                @csrf
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="name" value="{{$user['name']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last name</label>
                                <input type="text" class="form-control" name="last_name" value="{{$user['last_name']}}" placeholder="Your last name" disabled>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Telephone</label>
                                <input type="text" class="form-control" name="mobile_number" value="{{$user['mobile_number']}}" placeholder="Your telephone number" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Your email" value="{{$user['email']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['address']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Country of Living</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['country']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>State or Province</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['province']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['city']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>District</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['district']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Recidency Status</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['recidency_status']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['dob']}}" disabled>
                            </div>
                        </div>
                        <br>
                    </div>
                    <a href="{{route('edit',Auth::user()->id)}}" class="btn btn-dark">Change</a>

                    <!-- /row-->

                    <!-- /row-->
                </div>
            </div>
        </div>
        <!-- /box_general-->

        <!-- /row-->
        <p>

    </div>
    </div>
    <!-- /.container-fluid-->
</div>
<!-- /.container-wrapper-->
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <div class='copy'>© 2019 Hiru Marriage Proposals</div>

        </div>
    </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- Logout Modal-->
<div class="modal fade" id="log_out" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>


</body>


</html>
