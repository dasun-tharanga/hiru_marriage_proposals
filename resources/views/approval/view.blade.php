
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.ansonika.com/sparker/admin_section/user-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 14:14:09 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>SPARKER - Admin dashboard</title>
    @include('layouts.link')

</head>

<body class="fixed-nav sticky-footer" id="page-top">
@include('layouts.sidebar')
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Approval</li>
        </ol>
        <div class="box_general padding_bottom">
            <div class="header_box version_2">
                <h2><i class="fa fa-user"></i>Approval Details</h2>
            </div>
            @if( $user->expired_at  )

                <div class="alert alert-primary" role="alert">
                    Please Check The New Bank Slip.
                </div>
            @endif
            <div class="row">
                @if( $user->approved_at == '' and $user->expired_at == '' )
                    <div class="form-group">
                        <label>Bank Slip</label><br>
                        <img src="/images/{{$user->bank_slip}}" class="img-fluid" alt="Responsive image">
                    </div>
                @else

                @endif





                <div class="col-md-8 add_top_30">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="name" value="{{$user['name']}}" disabled>
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last name</label>
                                <input type="text" class="form-control" name="last_name" value="{{$user['last_name']}}" placeholder="Your last name" disabled>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Telephone</label>
                                <input type="text" class="form-control" name="mobile_number" value="{{$user['mobile_number']}}" placeholder="Your telephone number" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Your email" value="{{$user['email']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['address']}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Country</label>
                                <input type="text" class="form-control" placeholder="Your email" value="{{$user['country']}}" disabled>
                            </div>
                        </div>

                    </div>
                    <!-- /row-->
                    @if( $user->approved_at == '' and $user->expired_at == ''  )
                        <a href="{{ route('admin.users.approve', $user->id) }}"
                           class="btn btn-primary btn-sm">Enable</a>
                    @else
                    <a href="{{ route('admin.users.approve', $user->id) }}"
                       class="btn btn-primary btn-sm">Approve</a>
                    @endif
                    <!-- /row-->
                </div>
            </div>
        </div>
        <!-- /box_general-->

        <!-- /row-->

        </form>

    </div>
</div>
<!-- /.container-fluid-->
</div>
<!-- /.container-wrapper-->
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <div class="copy">© 2019 Hiru Marriage Proposals</div>
        </div>
    </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- Logout Modal-->
<div class="modal fade" id="log_out" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>


</body>


</html>
