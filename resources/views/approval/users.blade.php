
<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>Hiru - Admin Dashboard</title>
@include('layouts.link')

</head>

<body class="fixed-nav sticky-footer" id="page-top">
@include('layouts.sidebar')
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Users</li>
        </ol>
        <div class="box_general">
			<h4>Pending Users</h4>
			<div class="list_general">
                <ul>
                    <tbody>
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                    @forelse ($users as $user)
                    <li>
                        <figure><img src="/images/profile_images/{{ $user->profile_image }}" alt=""></figure>
                        <h4>{{ $user->name }} {{ $user->last_name }}
                            @if( $user->approved_at == '' and $user->expired_at == ''  )
                                <i class="pending">Pending</i>
                            @else
                                <i class="cancel">Disable</i>
                            @endif


                        </h4>
                        <ul class="booking_list">
                            <li><strong>ID</strong>{{ \App\Facades\GlobalServiceFacade::getPaddedId($user['id']) }}</li>
                            <li><strong>Email</strong>{{ $user->email }}</li>
                            <li><strong>Country</strong>{{ $user->country }}</li>
                            <li><strong>City</strong>{{ $user->city }}</li>
                        </ul>

                        <ul class="buttons">
                            <li><a href="{{route('approval-view',$user->id)}}" class="btn_1 gray approve"><i class="fa fa-fw fa-check-circle-o"></i> View</a></li>
                            <li><a data-toggle="modal" data-target="#delete_user" href="#delete_user" class="btn_1 gray delete"><i class="fa fa-fw fa-times-circle-o"></i> Delete</a></li>
                        </ul>
                    </li>

                    <div class="modal fade" id="delete_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                    <a class="btn btn-primary" href="{{route('approval-delete',$user->id)}}">Delete</a>

                                </div>
                            </div>
                        </div>
                    </div>

                    @empty
                        <li>
                            No Pending Users.
                        </li>
                    @endforelse

                </ul>
			</div>
		</div>

        </div>

    </div>
    </div>
    <!-- /.container-fluid-->
</div>
<!-- /.container-wrapper-->
<footer class="sticky-footer">

        <div class="text-center">
            <div class="copy">© 2019 Hiru Marriage Proposals</div>
        </div>

</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<div class="modal fade" id="log_out" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>


</body>


</html>
