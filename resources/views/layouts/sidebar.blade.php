<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-default fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.html"><img src="/img/logo.png" data-retina="true" alt="" width="165" height="36"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
@if(!auth()->user()->admin)
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
                <a class="nav-link" href="/home">
                    <i class="fa fa-fw fa-user"></i>
                    <span class="nav-link-text">My Profile</span>
                </a>
            </li>
                @if (auth()->user()->approved_at)
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
                        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMylistings" data-parent="#mylistings">
                            <i class="fa fa-fw fa-envelope-open"></i>
                            <span class="nav-link-text">Messages</span>
                        </a>
                        <ul class="sidenav-second-level collapse" id="collapseMylistings">
                            <li>
                                <a href="/messages/create">New Message</a>
                            </li>
                            <li>
                                <a href="/messages">All Messages</a>
                            </li>
                        </ul>
                    </li>
                @endif



            @else
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUser" data-parent="#users">
                        <i class="fa fa-user"></i>
                        <span class="nav-link-text">Users</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseUser">
                        <li>
                            <a href="/home">Pending Users</a>
                        </li>

                        <li>
                            <a href="/home/add">New User</a>
                        </li>

                    </ul>
                </li>
            @endif

        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">



            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#log_out">
                    <i class="fa fa-fw fa-sign-out"></i>Logout</a>
            </li>
        </ul>
    </div>
</nav>
<!-- /Navigation-->
