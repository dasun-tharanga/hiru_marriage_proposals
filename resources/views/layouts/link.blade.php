<!-- Bootstrap core JavaScript-->
<script src="/admin/vendor/jquery/jquery.min.js"></script>
<script src="/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="/admin/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<script src="/admin/vendor/chart.js/Chart.min.js"></script>
<script src="/admin/vendor/datatables/jquery.dataTables.js"></script>
<script src="/admin/vendor/datatables/dataTables.bootstrap4.js"></script>
<script src="/admin/vendor/jquery.selectbox-0.2.js"></script>
<script src="/admin/vendor/retina-replace.min.js"></script>
<script src="/admin/vendor/jquery.magnific-popup.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="/admin/js/admin.js"></script>
<!-- Custom scripts for this page-->
<script src="/admin/vendor/dropzone.min.js"></script>
<!-- Favicons-->
<link rel="shortcut icon" href="/admin/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" type="image/x-icon" href="/admin/img/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/admin/img/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/admin/img/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/admin/img/apple-touch-icon-144x144-precomposed.png">

<!-- Bootstrap core CSS-->
<link href="/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Main styles -->
<link href="/admin/css/admin.css" rel="stylesheet">
<!-- Icon fonts-->
<link href="/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Plugin styles -->
<link href="/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<link href="/admin/vendor/dropzone.css" rel="stylesheet">
<!-- Your custom styles -->
<link href="/admin/css/custom.css" rel="stylesheet">