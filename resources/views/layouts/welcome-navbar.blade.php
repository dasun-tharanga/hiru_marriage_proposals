
    <div id="logo">
        <a href="/" title="Sparker - Directory and listings template">
            <img src="/img/logo.png" width="165" height="55" alt="" class="logo_normal">
            <img src="/img/logo.png" width="165" height="55" alt="" class="logo_sticky">
        </a>
    </div>
    <ul id="top_menu">
        <li><a href="/register" class="btn_add" >Register</a>
        @if (Auth::user())
            <li>
                <a href="{{ route('login') }}" id="sign-in" class="login" title="Sign In" onclick="event.preventDefault();
                                                 document.getElementById('logout-in').submit();">
                    Sign In
                </a>
                <form id="logout-in" action="{{ route('login') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>

        @else
        <li>
            <a href="#sign-in-dialog" id="sign-in" class="login" title="Sign In">
                Sign In
            </a>
        </li>
        @endif

        <!--<li><a href="#" class="wishlist_bt_top" title="Your wishlist">Your wishlist</a></li>-->
    </ul>
    <!-- /top_menu -->
    <a href="#menu" class="btn_mobile">
        <div class="hamburger hamburger--spin" id="hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
    </a>
    <nav id="menu" class="main-menu">
        <ul>
            <li><span><a href="/">Home</a></span></li>
            <li><span><a href="/login">My Account</a></span></li>
            <li><span><a href="/register">Registration</a></span></li>
            <li><span><a href="#">Application</a></span></li>
            <li><span><a href="/filter">Members</a></span></li>
            <li><span><a href='/contact'>Contact Us</a></span></li>
        </ul>
    </nav>

