<!--<div class="media">
    <a class="pull-left" href="#">
        <img src="//www.gravatar.com/avatar/{{ md5($message->user->email) }} ?s=64"
             alt="{{ $message->user->name }}" class="img-circle">
    </a>
    <div class="media-body">
        <h5 class="media-heading">{{ $message->user->name }}</h5>
        <p>{{ $message->body }}</p>
        <div class="text-muted">
            <small>Posted {{ $message->created_at->diffForHumans() }}</small>
        </div>
    </div>
</div>-->

<li >
    <span>{{$message->created_at->diffForHumans()}}</span>
    <figure><img src="/images/profile_images/{{$message->user->profile_image }}" alt=""></figure>
    <h4>
        {{$message->user->name  }} {{$message->user->last_name  }}
    </h4>
    <p> {{  $message->body}} </p>
</li>