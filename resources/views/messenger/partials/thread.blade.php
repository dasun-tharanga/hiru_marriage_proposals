<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : ''; ?>

<!--<div class="media alert {{ $class }}">
    <h4 class="media-heading">
        <a href="{{ route('messages.show', $thread->id) }}">{{ $thread->subject }}</a>
        ({{ $thread->userUnreadMessagesCount(Auth::id()) }} unread)</h4>
    <p>
        {{ $thread->latestMessage->body }}
    </p>
    <p>
        <small><strong>Creator:</strong> {{ $thread->creator()->name }}</small>
    </p>
    <p>
        <small><strong>Participants:</strong> {{ $thread->participantsString(Auth::id()) }}</small>
    </p>
</div>-->


<li >
    <span>{{$thread->created_at->diffForHumans() }}</span>
    <figure><img src="/images/profile_images/{{$thread->creator()->profile_image }}" alt=""></figure>
    <h4><a href="{{ route('messages.show', $thread->id) }}">
            {{ $thread->creator()->name  }}
            {{ $thread->creator()->last_name }}

        </a>
        @if($thread->userUnreadMessagesCount(Auth::id()))
        <i class="unread">
            {{ $thread->userUnreadMessagesCount(Auth::id()) }} Unread
        </i>
     @endif
    </h4>
    <b>{{ $thread->subject }}</b>
    <p> {{ str_limit($thread->latestMessage->body, 50)}} </p>
</li>