
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.ansonika.com/sparker/contacts-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 14:13:00 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="SPARKER - Premium directory and listings template by Ansonika.">
    <meta name="author" content="Ansonika">
    <title>Hiru | Terms and Conditions</title>

</head>

<body>
@include('layouts.theme')
<div id="page">

    <header class="header_in is_sticky menu_fixed">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12">
                    @include('layouts.other-navbar')
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </header>
    <!-- /header -->

    <div class="sub_header_in sticky_header">
        <div class="container">
            <h1>Terms and Conditions</h1>
        </div>
        <!-- /container -->
    </div>
    <!-- /sub_header -->

    <main>
        <!-- /map -->
        <div class="container margin_60_35 box">
            <p><strong><u>Hirumarriageproposals.lk</u></strong></p>
          <p><span lang="SI-LK" xml:lang="SI-LK">හිරු  මංගල සේවයේ  වෙබ් අඩවිය වන </span>Hirumarriageproposals.lk <span lang="SI-LK" xml:lang="SI-LK">වෙත  ඔබ සාදරයෙන්  පිළිගනිමු.</span> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">හිරු  ඩේටා බෑන්ක්  ආයතනය හිරු  මංගල සේවය  තුලින් ජනතාව  වෙත සිය සේවාව  සැපයීම සඳහා </span> Hirumarriageproposals.lk<span lang="SI-LK" xml:lang="SI-LK"> වෙබ් අඩවිය තොරතුරු  සැපයීමේ මාධ්‍යයක්  ලෙස ප්‍රයෝජනය  කර ගනියි. </span> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">ඔබ </span>Hirumarriageproposals.lk<span lang="SI-LK" xml:lang="SI-LK"> වෙබ් අඩවියට  පිවිසෙන්නේ  නම් පහත සඳහන්  කොන්දේසි පිළිගැනීමට  බැඳී සිටිය  යුතුය.</span> </p>
          <p> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">මෙම වෙබ්  අඩවිය  භාවිතයට අවසර ඇති  අය.</span> </p>
          <ul><li><span lang="SI-LK" xml:lang="SI-LK">ඔබගේ වයස  අවුරුදු 18 කට  වැඩිවිය  යුතුය.</span> </li>
            <li><span lang="SI-LK" xml:lang="SI-LK">ගිවිසුමකට  ඇතුළත් වීමට  නීතිමය  බාධාවන් නොතිබිය  යුතුය. </span> </li>
            <li><span lang="SI-LK" xml:lang="SI-LK">පහතින්  සඳහන් වන  කොන්දේසි  සියල්ල  පිළිගැනීමට  එකඟ විය  යුතුය. </span> </li>
            <li><span lang="SI-LK" xml:lang="SI-LK">මෙම වෙබ්  අඩවියට  පිවිසෙන යම්  අයෙකු පහත  සඳහන් ‍කොන්දේසි  එකක් හෝ  සියල්ලම  පිළිගැනීමට  සූදානම්  නැතිනම් මෙම  වෙබ් අඩවියට  ඇතුළුවීමෙන්  වැළකී සිටිය  යුතුය. </span> </li>
          </ul>
          <p> </p>
          <p><strong><u><span lang="SI-LK" xml:lang="SI-LK">ඔබ </span>Hirumarriageproposals.lk<span lang="SI-LK" xml:lang="SI-LK"> වෙබ් අඩවියට  ඇතුළු වන්නේ  නම් පහතින්  සඳහන් කොට ඇති  සියළුම  කොන්දේසි  පිළිගන්නා  බවට පිළියන්දල,  මාවිත්තර,  ආවාස පාරේ අංක  6 ස්ථානයේ ලියාපදිංචි  කොට ඇති </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය හා නීත්‍යානුකූල  ගිවිසුමකට  මෙයින්  ඇතුලත් වේ. </span></u></strong> </p>
          <p> </p>
          <ul>
            <li><strong>1.     <u><span lang="SI-LK" xml:lang="SI-LK">අපගේ  සේවය</span></u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">මෙම  සේවාව මගින්  කෙරෙන්නේ  විවාහ අපේක්ෂයින්ට  තමන්ගේ රුචි  පරිදි කෙනෙකු  තෝරා ගැනීමට  අවස්ථාවක්  ලබා දීම පමණි. ඉන්පසු  තවදුරටත්  කටයුතු  කරගෙන යාම ඔබ  හා එකී  පාර්ශවන්ගේ  උවමනාව,  කැමැත්ත හා  එකඟතාවය මත  පමණක් සිදුවේ.  එවැනි  සම්බන්ධතාවයක්  පවත්වාගෙන  යාම සඳහා  කිසිම අයුරක  අනුබල දීමක්  හෝ බලපෑමක් </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය  මගින් සිදු  නොකෙ‍රේ. යම්  සම්බන්ධතාවයක්  හේතුකොටගෙන  කිසියම් පාර්ශවයකට  සිදුවන ගැටළු,  පාඩු හෝ මූල්‍යමය  අවාසි  සම්බන්ධයෙන් </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය  වගකියනු  නොලැබේ.</span> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">තවද  මෙම මංගල  සේවයේ ලියාපදිංචි  වී ඇති විවාහ  අපේක්ෂකයින්ගේ  විවාහක,  අවිවාහකබව,  පදිංචි  ලිපිනය, රැකියාව  ඇතුළු  සම්පූර්ණ  විස්තර වල සත්‍ය  අසත්‍ය‍ භාවය  සොයා බැලීමට </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය කිසිදු  අන්දමේ  වගකීමකට බැඳී  නොමැත. මෙම  වෙබ් අඩවිය  තුලින්  දැනගන්නා යම්  කෙනෙකු සමඟ  සම්බන්ධතාවයක්  පටන් ගැනීමට  පෙර ඔහු හෝ ඇය  සපයා ඇති  සියලු  තොරතුරු වල  සත්‍ය අසත්‍ය  තාවය  සොයාගැනීම  තමාගේ කාර්ය  භාරය හා වගකීම  බව ඔබ පිළිගත  යුතුය.  </span> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">මෙම වෙබ්  අඩවිය තුළින්  හඳුනාගන්නා  කිසිවකු සමඟ විවාහයට  පෙර කිසිම  මුදල්  ගනුදෙනුවකට  සම්බන්ධ නොවන  බවටද ඔබ  එකඟවිය යුතු  වේ. </span> </p>
          <ul>
            <li><strong>2.     <u><span lang="SI-LK" xml:lang="SI-LK">වෙබ්  අඩවිය භාවිතය</span></u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">ඔබ  ඇතුළු සියළුම  සාමාජිකයන්ට  වෙබ් අඩවිය  භාවිතා කිරීම  සඳහා සාමාජික  අංකයක් හා </span>PASSWORD <span lang="SI-LK" xml:lang="SI-LK">එකක්  ආයතනය මගින්  ලබා දේ. </span> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">ඉන්  පසු තමන්ට  රිසි </span>PASSWORD<span lang="SI-LK" xml:lang="SI-LK"> එකක්  යොදාගෙන  තමන්ගේ ගිණුම  පවත්වාගෙන  යාම සාමාජිකයාගේ  වගකීම වේ.  තමන්ගේ ‍</span>Profile <span lang="SI-LK" xml:lang="SI-LK">එකේ  වෙනස්කම් කර  ගැනීම හා  ගිණුම  ආරක්ෂාකාරීව  පවත්වාගෙන  යාම තමන්ගේ  වගකීම බව  සාමාජිකයා  පිළිගතයුතු  වේ. </span>Password <span lang="SI-LK" xml:lang="SI-LK">එක  සුරැකිව තබා  ගත යුතු අතර  තමන්ගේ  ගිණුමේ යම්  වෙනසක්  සිදුවුවහොත්  එහි වගකීම  සාමාජිකයා  බාරගත යුතු  වේ.  </span> </p>
          <p> </p>
          <ul>
            <li><strong>3.     <u><span lang="SI-LK" xml:lang="SI-LK">නියමිත  මුදල් ගෙවා  මෙම වෙබ්  අඩවියට  ලියාපදිංචි  වීමෙන් ඔබට  ලැබෙන ප්‍රතිලාභ</span></u></strong> </li>
            <li>1.      <span lang="SI-LK" xml:lang="SI-LK">ඔබගේ  විස්තර සහිතව  මංගල  යෝජනාවක්  මෙහි  පළකර ගැනීමට  හැකිවීම. ඔබට  කටයුත්තක්  තීන්දු වන  තෙක් එය මෙහි  තබාගත හැක. </span> </li>
            <li>2.      <span lang="SI-LK" xml:lang="SI-LK">මෙම වෙබ්  අඩවියේ පලවී  ඇති </span>Profile <span lang="SI-LK" xml:lang="SI-LK">වල දුරකථන  අංක දින 15 කට 20  බැගින් මාස 6 ක  කාලයක් දක්වා  වෙබ්  අඩවියෙන්ම  ලබාගැනීමේ  පහසුකම.</span> </li>
            <li>3.      <span lang="SI-LK" xml:lang="SI-LK">ඔබ  ලබාගන්නා  සමහර දුරකථන  අංක  සමහරවිටෙක ක්‍රියාවිරහිත  වී හෝ ප්‍රතිචාර  නොදී හෝ තිබිය  හැක. සමහරක්  අය දැනට යෝජනා  කතාකරගෙන යන  අය විය හැක.  එවැනි  අවස්ථාවන් ගැන  වෙන වෙනම සොයා  බලා හේතු  කියාසිටීමට  ආයතනය වගකීමෙන්  බැඳී නොමැත.  දින 15 කට  දුරකථන අංක 20  ක්ම  ලබාගැනීමට  ඔබට අවස්ථාව  සළසා දී ඇත්තේ  එවැනි සම්බන්ධ  කරගත නොහැකි  අංක ඇති බව  සැලකිල්ලට ගනිමිනි. </span> </li>
            <li>4.      <span lang="SI-LK" xml:lang="SI-LK">දෛවඥ සේවය  සඳහා මුදල්  ගෙවා  ලියාපදිංචි වන  සාමාජිකයන්ට  මාස 6 ක් දක්වා  කේන්දර ගළපා  යෝජනා  තැපෑලෙන්  එවීමක්  සිදුවේ. මේ  සඳහා අත්සන්  කරන ලද  ඉල්ලුම්පතක්  අපට ලබාදිය  යුතු වේ. </span> </li>
            <li>5.      <span lang="SI-LK" xml:lang="SI-LK">මාස 6 කට  පසුව අංක 2 න්  හා 4 න්  දැක්වෙන සේවාවන්  තවදුරටත්  ලබාගැනීමට ඔබට  අවශ්‍ය නම්  අලුත් කර  ගැනීමක්  සිදුකළ යුතු  වේ. ඒ සඳහා  ගෙවිය යුතු  ගාස්තු  ආයතනයට  ඇමතුමක් ලබා  දීමෙන් දැනගත  හැකිවේ. </span> </li>
            <li>6.       ආයතනයේ ප්‍රතිපත්ති අනුව සාමාඡිකයකුගේ දුරකථන අංකය ලබා ගැනීමට අවසර ඇත්තේ
            තවත් සාමාජිකයෙකුට පමණි. පිටස්තර අයට ඒවා ලබා ගැනීමට අවකාශ නැත. සාමාජිකයන්ගේ දුරකථන අංක ප්‍රදර්ශණය වීම නිසා බාහිර පුද්ගලයින්ගෙන් සාමාඡිකයින්ට හිරිහැර වන අවස්ථා ඇති බැවින් ආයතනය එම පියවර ගෙන ඇත. එබැවින් වෙබ් අඩවියේ ඇති සෑම යෝජනාවකම දුරකථන අංක ප්‍රදර්ශණය කර නොමැත.
              <div align="left">එහෙත් සමහරක් සාමාඡිකයින්ගේ බලවත් ඉල්ලීම නිසා රු 3000/- ක අමතර මුදලක් ගෙවීමෙන් මාස 6ක කාලයකට දුරකථන අංකය ප්‍රදර්ශණය කිරීමට අවස්ථාව ලබාදී ඇත. මෙලෙස බාහිර අයට දුරකථන අංකය ලැබීමෙන් එම සාමාජිකයින්ට යම් අපහසුතාවයකට මුහුණ දීමට සිදු වුවහොත් ඒ ගැන හිරු ඩේටා බෑන්ක් ආයතනය වග කියනු නොලැබේ.
                <div align="left"> බාහිර පුද්ගලයින්ට සාමාජිකයින් හා සම්බන්ද වීමේ අවස්ථාව අප විසින් අවහිර කර නොමැත. බාහිර පුද්ගලයින්ට සාමාජිකයින් වෙත නොමිලේ ලිපි යැවීමේ අවස්ථාව දිගටම ක්‍රියාත්මකයි.</div>
              </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     <p></p>

            </li>
          </ul>
          <ul><li><strong>4.     <u><span lang="SI-LK" xml:lang="SI-LK">පිටපත්  කිරීම</span></u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">මෙම වෙබ්  අඩ‍වියේ ඇති  සියලුම දත්ත  හා ඡායාරූප වල  අයිතිය </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය සතුවේ.  මෙහි එන දත්ත  හා ඡායාරූප  කිසිවක් කුමන  අන්දමකට හෝ  පිටපත්  කිරීමට කිසිවෙකුටත්  අවසර නොමැත.</span> </p>
          <p> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">එය  සම්පූර්ණයෙන්ම  නීති විරෝධි  ක්‍රියාවකි.  මෙහි පලවී ඇති  යම්  ඡායාරූපයක්  පිටපත් කර ලබා  ගැනීමෙන් එම  සාමාජිකයාට යම්  ගැටලුවකට  මුහුණු දීමට  සිදුවුවහොත්  එම වගකීමෙන් </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය නිදහස්  වන අතර එම ක්‍රියාව  සිදු කළ  පුද්ගලයා ඒ  සම්බන්ධ සම්පූර්ණ  වගකීම  බාරගැනීමට  බැඳී ඇත.  එවැනි තැනැත්තෙකුට  විරුද්ධව  නීත්‍යානුකූල  පියවර ගැනීමට  හිරු ඩේටා  බැන්ක් අයතනයට  බලය ඇත.</span> </p>
          <p> </p>
          <ul>
            <li><strong>5.       <u><span lang="SI-LK" xml:lang="SI-LK">වෙබ්  අඩවියේ පලවී  ඇති වෙළඳ  දැන්වීම්</span></u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">අනෙකුත්  ඕනෑම වෙබ්  අඩවියක මෙන්  මෙම වෙබ්  අඩවියේද වෙළඳ  දැන්වීම්  පලකර ඇත. මෙහි  පලකර ඇති සෑම  වෙළඳ  දැන්වීමක්ම  පාලනය කරනු  ලබන්නේ </span>Google <span lang="SI-LK" xml:lang="SI-LK">ආයතනය  විසිනි. එම  දැන්වීම් වල  අයිතිය ඔවුන්</span> <span lang="SI-LK" xml:lang="SI-LK">සතුය.  එම දැන්වීම්  සම්බන්ධව ක්‍රියාකිරීම  ඔබගේ තනි  කැමැත්ත ඇතිව  සිදුවේ. ඒ සම්බන්ධව  කිසිදු  වගකීමක්  දැරීමට </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය නෛතිකව  බැඳි නොමැත. </span> </p>
          <ul>
            <li><strong>6.     <u><span lang="SI-LK" xml:lang="SI-LK">තොරතුරු  වල රහස්‍යභාවය</span></u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">සාමාජිකයෙකු  විසින් </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනයට සපයා  ඇති කිසිම  තොරතුරක්  පිටතට ලබා දීමට  ආයතනය බැඳී  නොමැත. එහෙත්  නීතියේ අවශ්‍යතාවයක්  මත එසේ කිරීමට  සිදුවුවහොත්  එයට අනුකූල  වීමට අප බැඳී  සිටී. </span> </p>
          <p> </p>
          <ul>
            <li><strong>7.     <u><span lang="SI-LK" xml:lang="SI-LK">සාමාජිකත්වය  අහෝසි කිරිම</span></u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">ඕනෑම  සාමාජිකයෙකුට  ඕනෑම  අවස්ථාවකදි  තමන්ගේ  සාමාජිකත්වය  අහෝසි කරගත  හැක. ඒ සඳහා  ආයතනයට ඇමතුමක්  දී තමන්ගේ  අනන්‍යතාවය  ඔප්පු කළ යුතු  වේ. </span> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">එලෙසම  පහත සඳහන්  කුමන ක්‍රියාවක්  නිසා හෝ ඔබගේ  සාමාජිකත්වය ‍ඉවත්  කිරීමට </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනයට බලය  ඇත.</span> </p>
          <ul>
            <li>1.      <span lang="SI-LK" xml:lang="SI-LK">තමන්ගේ  නොවන දුරකථන  අංක ඇතුලත්  කිරීම</span> </li>
            <li>2.      <span lang="SI-LK" xml:lang="SI-LK">තමන්ගේ  නොවන ඡායාරූප  පලකිරීම</span> </li>
            <li>3.      <span lang="SI-LK" xml:lang="SI-LK">වෙබ්  අඩ‍වියේ ඇති  දත්ත හා  ඡායාරූප  පිටපත් කිරීම  හෝ සොරකම්  කිරීම</span> </li>
            <li>4.      <span lang="SI-LK" xml:lang="SI-LK">දුරකථන  ඇමතුම් වලට  යහපත් ප්‍රතිචාර  නොදීම</span> </li>
            <li>5.      <span lang="SI-LK" xml:lang="SI-LK">තවමත්  කටයුත්තක්  තීන්දු නොවී  තිබියදී හා තමන්ගේ </span>Profile <span lang="SI-LK" xml:lang="SI-LK">එක වෙබ්</span> <span lang="SI-LK" xml:lang="SI-LK">අඩවියේ  තබාගෙන  සිටියදී  වෙනත්  සාමාජිකයෙකු කතා  කල විට ''අපට  එකක් හරි  ගිහිල්ලා'' යැයි  කීම. </span> </li>
            <li>6.      <span lang="SI-LK" xml:lang="SI-LK">ආයතයනය  විසින්  සාමාජිකයාගේ  ලිපිනයට  තැපැල් මගින්  යවනු ලබන ලිපි  ආපසු ඒම</span> </li>
            <li>7.      <span lang="SI-LK" xml:lang="SI-LK">මෙම  සේවය තුළින්  හඳුනාගන්නා  අය සමඟ මුදල්  ගනු‍දෙනු වලට  සම්බන්ධ වීම.</span> </li>
            <li>8.      <span lang="SI-LK" xml:lang="SI-LK">මෙම  ගිවිසුමේ  සඳහන්  කොන්දේසි  එකක් හෝ එයට  වැඩි ගණනක් කඩ  කිරිම.</span> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">එමෙන්ම  යම්  සාමාජිකයෙකු විසින්  තවත්  සාමාජිකයෙකුට  විරුද්ධව අප  වෙත ඉදිරිපත්  කරනු ලබන  පැමිණිල්ලක්  සම්බන්ධව නිසි  විභාගයක්  පැවැත්වීමෙන්  පසුව එවැනි  සාමාජිකයන්ගේ  සාමාජිකත්වය  අහෝසි කිරිමට </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනයට බලය  ඇත. </span> </p>
          <ul>
            <li><strong>8.     <u><span lang="SI-LK" xml:lang="SI-LK">තමන්ගේ  නොවන විස්තර  ඇතුලත් </span>කිරිම</u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">යම්  සාමාජිකයෙකු  විසින්  තමන්ගේ නොවන  ඡායාරූපයක්  හෝ දුරකථන  අංකයක්  තමන්ගේ </span>Profile<span lang="SI-LK" xml:lang="SI-LK"> එකට  ඇතුල් කොට  ඇත්නම් ඒ බව  දැනගන්නා යම්  තැනැත්තෙකුට  එම කරුණ  සම්බන්ධව </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනයට  දුරකථනයෙන්  දැනුම් දීමට  හැකි අතර </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය වහාම ක්‍රියාත්මක  වී එවැනි  තොරතුරු වෙබ්  අඩවියෙන්  ඉවත් කිරීමට  අවශ්‍ය පියවර  ගනු ඇත. </span> </p>
          <p>&nbsp;</p>
          <p><span lang="SI-LK" xml:lang="SI-LK">මෙවැන්නක්  කිරීමෙන්  ඡායාරූපයේ හෝ  දුරකථනයේ  සැබෑ  අයිතිකරුට  විඳින්නට වන  ඕනෑම අන්දමේ ගැටළුවක්  හෝ පාඩුවක්  සම්බන්ධ  සම්පූර්ණ  වගකීම එම  සාවද්‍ය ‍තොරතුරු  තමන්ගේ </span>Profile<span lang="SI-LK" xml:lang="SI-LK"> එකට  ඇතුලත් කරගෙන  සිටි  සාමාජිකයා  විසින් භාරගත  යුතු අතර  හිරු  ඩේටාබෑන්ක්  ආයතනය ඒ  සියල්ලෙන්ම  නිදහස් වේ.  එවැනි      තැනැත්තෙකුට  විරුද්ධව  නීතිමය පියවර  ගැනීමට  හිරු ඩේටා  බැන්ක්  ආඅයතනයට බලය  ඇත.</span> </p>
          <p> </p>
          <ul>
            <li><strong>9.     <u><span lang="SI-LK" xml:lang="SI-LK">මුදල්  ආපසු දීමේ ප්‍රතිපත්තිය</span></u></strong></li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">ඉහත  අංක 7 හා 8 යටතේ  දක්වා ඇති එක්  කරුණක් හෝ  කරුණු  කිහිපයක්  නිසා හෝ  සාමාජිකත්වය  අහෝසි වූ පුද්ගලයෙකුට  ඔහු හෝ ඇය  විසින් ආයතනයට  ගෙවූ මුදල්  ආපසු ගෙවීමට </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය බැඳී  නොමැත. </span> </p>
          <p><span lang="SI-LK" xml:lang="SI-LK">එහෙත්  මුදල් ගෙවා  ලියාපදිංචි  වන ඕනෑම සාමාජිකයෙකුට  තමන්ට සතුටු  දායක සේවයක්  ආයතනය මගින්  සැලසී නැතැයි සිතනවානම්  මුදල් ගෙවූ  දින සිට දින 10  ක් ඇතුලත ගෙවූ  මුදල් ආපසු  ලබාදෙන ලෙස  ආයතනයට  ඉල්ලීමක් කල  හැක. ආයතනය  විසින්  සාමාජිකයාගේ  ගිණුම පරීක්ෂා  කොට අන් අයගේ  දුරකථන අංක  කිසිවක් ලබාගෙන  නොමැති බවට  තහවුරු  කරගැනීමෙන්  පසු එම මුදල්  ආපසු ලබාදීමට  එකග වේ. </span></p>
          <ul>
            <li><strong>10</strong> <strong><u>ගිවිසුමේ සඳහන් කොන්දේසි</u></strong></li>
          </ul>
          <p>මෙම ගිවිසුමේ සඳහන් කොන්දේසි කිසිම පූර්ව දැනුම් දීමකින් තොරව වෙනස් කිරීමේ හෝ අළුතින් කොන්දේසි එකතු කිරීමේ බලය exceldatabank ආයතනය  සතුය. එබැවින් කලින් කලට මෙම කොන්දේසි පරික්ෂා කිරීම ඔබගේ වගකීම වේ.</p>
          <p>&nbsp;</p>
          <ul>
            <li><strong>11. <u><span lang="SI-LK" xml:lang="SI-LK">බලපවත්වන  නීතිය</span></u></strong> </li>
          </ul>
          <p><span lang="SI-LK" xml:lang="SI-LK">ඕනෑම  සාමාජිකයෙකු </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය වෙත  ලියාපදිංචි  වූ විට ඔහු හෝ  ඇය </span>exceldatabank<span lang="SI-LK" xml:lang="SI-LK"> ආයතනය සමඟ මෙම  ලියවිල්ලේ  සඳහන් කරුණු  කාරණාවලට  යටත්ව  ගිවිසුමකට  එළඹෙන බව  සැලකෙන අතර එය  පිළියන්දලදී  නීතිගත වූ බවට  සැලකේ. </span> </p>
          <p>&nbsp;</p>
          <ul>
            <li><strong>12. <u><span lang="SI-LK" xml:lang="SI-LK">ආරවුල්  බේරුම් කිරීම</span></u></strong> </li>
          </ul>
          <p>Hirumarriageproposals.lk <span lang="SI-LK" xml:lang="SI-LK">වෙබ්  අඩවිය හා හිරු  මංගල සේවය  සම්බන්ධයෙන්  වන සියලුම  ආරවුල් වර්ෂ 1995  අංක 11 ආරවුල්  බේරුම්  කිරීමේ පනත යට‍තේ  බේරුම්  කරුවන්  තුන්දෙනෙකු (3)  ගෙන්  සැදුම්ලත් බේරුම්කරන  සභාවක්  ඉදිරියේදී  කොළඹ දී විභාගයට  ගැනීමට ඔබ හා  හිරු ඩේ‍ටා  බැන්ක් ආයතනය  මෙයින් එකග  වේ. </span> </p>
          <p>&nbsp;</p>
          <p><strong><u>Settlement of Disputes </u></strong></p>
          <p>You and  Excel  data bank here by agrees that any dispute arising out of or in any way  effecting this agreement will be referred to arbitration before an arbitration  panel comprising of three (3) members under the Arbitration Act no 11 of 1995  at Colombo, Sri Lanka.    </p>
        </div>
        <!-- /container -->
    </main>
    <!--/main-->

    @include('layouts.footer')
    <!--/footer-->
</div>
<!-- page -->
<!-- Sign In Popup -->
@include('layouts.popup')

<div id="toTop"></div><!-- Back to top button -->

<!-- COMMON SCRIPTS -->
<script src="js/common_scripts.js"></script>
<script src="js/functions.js"></script>
<script src="assets/validate.js"></script>

<!-- COLOR SWITCHER  -->
<script src="js/switcher.js"></script>

</body>

</html>
