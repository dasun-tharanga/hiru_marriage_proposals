
<!DOCTYPE html>
<html lang="en">



<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Hiru Marriage Proposals</title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

	<!-- GOOGLE WEB FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

	<!-- BASE CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/vendors.css" rel="stylesheet">

	<!-- ALTERNATIVE COLORS CSS -->
	<link href="#" id="colors" rel="stylesheet">

</head>

<body>
@include('layouts.theme')

<div id="page">
        <header class="header menu_fixed">
                @include('layouts.welcome-navbar')
        </header>


	<!-- /header -->

	<main>
		<section class="hero_single version_5">
			<div class="wrapper">
				<div class="container">
					<div class="row justify-content-center pt-lg-5">
						<div class="col-xl-5 col-lg-6">
							<h3>Find Your Dream!</h3>
							<p>Discover top rated hotels, shops and restaurants around the world</p>
							<form method="GET" action="/filteroption2">
								@csrf

								<div class="custom-search-input-2">
									<select class="wide"  name="gender" {{old('gender')}}>
										<option value='Male'>Male</option>
										<option value="Female">Female</option>
									</select>
									<input type="submit" value="Search">
								</div>
							</form>
						</div>
						<div class="col-xl-5 col-lg-6 text-right d-none d-lg-block">
							<img src="/img/graphic_home_2.svg" alt="" class="img-fluid">
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- /hero_single -->

		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>Photo Gallery</h2>

				</div>
				<div class="grid-gallery">
					<ul class="magnific-gallerys">
						@foreach($user as $users)
							<a href="{{route('view',$users->id)}}" target="_self" ><img src="/images/profile_images/{{$users->profile_image}}" width="100" height="100" alt=""></a>

						@endforeach


					</ul>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->



		<div class="call_section image_bg">
			<div class="wrapper">
				<div class="container margin_80_55">
					<div class="main_title_2">
						<span><em></em></span>
						<h2>How it Works</h2>
						<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="box_how wow">
								<i class="pe-7s-search"></i>
								<h3>Search Locations</h3>
								<p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
								<span></span>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box_how">
								<i class="pe-7s-info"></i>
								<h3>View Location Info</h3>
								<p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
								<span></span>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box_how">
								<i class="pe-7s-like2"></i>
								<h3>Book, Reach or Call</h3>
								<p>An nec placerat repudiare scripserit, temporibus complectitur at sea, vel ignota fierent eloquentiam id.</p>
							</div>
						</div>
					</div>
					<!-- /row -->
					<p class="text-center add_top_30 wow bounceIn" data-wow-delay="0.5"><a href="register.html" class="btn_1 rounded">Register Now</a></p>
				</div>
			</div>
			<!-- /wrapper -->
		</div>
		<!--/call_section-->
	</main>
	<!-- /main -->

@include('layouts.footer')
	<!--/footer-->
</div>
<!-- page -->

@include('layouts.popup')

<div id="toTop"></div><!-- Back to top button -->

<!-- COMMON SCRIPTS -->
<script src="js/common_scripts.js"></script>
<script src="js/functions.js"></script>
<script src="assets/validate.js"></script>

<!-- COLOR SWITCHER  -->
<script src="js/switcher.js"></script>

</div>

</body>

</html>
