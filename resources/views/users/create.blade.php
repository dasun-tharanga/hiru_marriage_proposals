
<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>Hiru - Admin dashboard</title>
    @include('layouts.link')

</head>

<body class="fixed-nav sticky-footer" id="page-top">
@include('layouts.sidebar')
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Users</li>
        </ol>
        <div class="box_general padding_bottom">
            <div class="header_box version_2">
                <h2><i class="fa fa-user"></i>New User</h2>
            </div>
            @if (session('message'))
                <div class="alert alert-success" role="alert">
                    {{ session('message') }}
                </div>
            @endif
            <div class="row">
                <form method="POST" action="/home/user" enctype='multipart/form-data'>
                    @csrf
                    <div class="form-group">
                        <label>First Name</label>
                        <input required class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text"  value="{{ old('name') }}" name="name">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input required class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" type="text"  value="{{ old('last_name') }}" name="last_name">
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Your Email</label>
                        <input required class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email"  value="{{ old('email') }}" name="email">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <i class="icon_mail_alt"></i>
                    </div>
                    <div class="form-group">
                        <label>Mobile Number</label>
                        <input required class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" type="text" maxlength="10"  value="{{ old('mobile_number') }}" name="mobile_number">
                        @if ($errors->has('mobile_number'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-mobile"></i>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea required class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" rows="5" type="text"  value="{{ old('address') }}" name="address"></textarea>
                        @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Country of Living</label>
                        @include('layouts.country')
                        @if ($errors->has('country'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                        @endif

                    </div>
                    <div class="form-group">
                        <label>State or Province</label>
                        <input required class="form-control{{ $errors->has('province') ? ' is-invalid' : '' }}" type="text"  value="{{ old('province') }}" name="province">
                        @if ($errors->has('province'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-location-pin"></i>
                    </div>
                    <div class="form-group">
                        <label>City</label>
                        <input required class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" type="text"  value="{{ old('city') }}" name="city">
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-location-pin"></i>
                    </div>
                    <div class="form-group">
                        <label>District</label>
                        <input class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" type="text"  value="{{ old('district') }}" name="district">
                        @if ($errors->has('district'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('district') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-location-pin"></i>
                    </div>
                    <div class="form-group">
                        <label>Recidency Status</label>
                        <input class="form-control{{ $errors->has('recidency_status') ? ' is-invalid' : '' }}" type="text"  value="{{ old('recidency_status') }}" name="recidency_status">
                        @if ($errors->has('recidency_status'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('recidency_status') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Date Of Birth</label>
                        <input class="form-control{{ $errors->has('dob') ? ' is-invalid' : '' }}" type="date"  value="{{ old('dob') }}" name="dob">
                        @if ($errors->has('dob'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-calendar"></i>
                    </div>
                    <div class="form-group">
                        <label>Age</label>
                        <input class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" type="text"  value="{{ old('age') }}" name="age">
                        @if ($errors->has('age'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('age') }}</strong>
                                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control" {{old('gender')}}>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        @if ($errors->has('gender'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Mother Tongue</label>
                        <select name="mother_tongue" class="form-control" {{old('mother_tongue')}}>
                            <option value="Sinhala">Sinhala</option>
                            <option value="Tamil">Tamil</option>
                            <option value="English">English</option>
                        </select>
                        @if ($errors->has('mother_tongue'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('mother_tongue') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Religion</label>
                        <select name="religion" class="form-control" {{old('religion')}}>
                            <option value='Anglican'>Anglican</option>
                            <option value='Buddhist' selected>Buddhist</option>
                            <option value='Catholic'>Catholic</option>
                            <option value='Christian'>Christian</option>
                            <option value='Hindu'>Hindu</option>
                            <option value='Islam'>Islam</option>
                            <option value='Methodist'>Methodist </option>
                            <option value='Sevenday'>Sevenday</option>
                        </select>
                        @if ($errors->has('religion'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('religion') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Caste</label>
                        <select name="caste" class="form-control" {{old('caste')}}>
                            <option value='Barathas'>Barathas</option>
                            <option value='Bathgama'>Bathgama</option>
                            <option value='Berawa'>Berawa</option>
                            <option value='Bodhi'>Bodhi</option>
                            <option value='Brahmana'>Brahmana</option>
                            <option value='Choliya'>Choliya</option>
                            <option value='Dewa'>Dewa</option>
                            <option value='Do not know'>Do not know</option>
                            <option value='Durawa'>Durawa</option>
                            <option value='Govi'>Govi</option>
                            <option value='Hakuru'>Hakuru</option>
                            <option value='Hunu'>Hunu</option>
                            <option value='Karawa'>Karawa</option>
                            <option value='Kumbal'>Kumbal</option>
                            <option value='Neketh'>Neketh</option>
                            <option value='Radala'>Radala</option>
                            <option value='Rajaka'>Rajaka</option>
                            <option value='Salagama'>Salagama</option>
                            <option value='Soli'>Soli</option>
                            <option value='Vishva'>Vishva</option>
                            <option value='Wellalan'>Wellalan</option>
                        </select>

                        @if ($errors->has('caste'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('caste') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Height(ft)</label>
                        <input class="form-control{{ $errors->has('height') ? ' is-invalid' : '' }}" type="text"  value="{{ old('height') }}" name="height">
                        @if ($errors->has('height'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('height') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Complexion</label>
                        <select name="complexion" class="form-control" {{old('complexion')}}>
                            <option value='Dark'>Dark</option>
                            <option value='Fair'>Fair</option>
                            <option value='Medium'>Medium</option>
                            <option value='Very Fair'>Very Fair</option>
                        </select>

                        @if ($errors->has('complexion'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('complexion') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Body Type</label>
                        <select name="body_type" {{old('body_type')}} class='form-control'>
                            <option value='Athletic'>Athletic</option>
                            <option value='Average'>Average</option>
                            <option value='Heavy'>Heavy</option>
                            <option value='Slim'>Slim</option>
                        </select>

                        @if ($errors->has('body_type'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('body_type') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Marital Status</label>
                        <select name="marital_status" {{old('marital_status')}} class='form-control'>
                            <option value='Divorced'>Divorced</option>
                            <option value='Divorced after Registration only'>Divorced after Registration only</option>
                            <option value='Never Married'>Never Married</option>
                            <option value='Separated'>Separated</option>
                            <option value='Widowed'>Widowed</option>
                        </select>

                        @if ($errors->has('marital_status'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('marital_status') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Children</label>
                        <select name="children" {{old('children')}} class='form-control'>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>

                        @if ($errors->has('children'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('children') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Education Category</label>
                        <select name="education_category" {{old('education_category')}} class="form-control">
                            <option value='A/L + any Diploma Course'>A/L + any Diploma Course</option>
                            <option value='A/L Passed'>A/L Passed</option>
                            <option value='Any Degree'>Any Degree</option>
                            <option value='O/L Passed'>O/L Passed</option>
                            <option value='OL + Diploma'>OL + Diploma</option>
                            <option value='Up to O/L'>Up to O/L</option>
                        </select>

                        @if ($errors->has('education_category'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('education_category') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Education in Detail</label>
                        <textarea class="form-control{{ $errors->has('education_detail') ? ' is-invalid' : '' }}" rows="5" type="text"  value="{{ old('education_detail') }}" name="education_detail"></textarea>
                        @if ($errors->has('education_detail'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('education_detail') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Job Category</label>
                        <select name="job_category" {{old('job_category')}} class="form-control">
                            <option value='Accounting Personnel'>Accounting Personnel</option>
                            <option value='Armed Forces'>Armed Forces</option>
                            <option value='Big Business Owners'>Big Business Owners</option>
                            <option value='Carftsman'>Carftsman</option>
                            <option value='Chefs / Cooks / House Keepers (Hotel Trade)'>Chefs / Cooks / House Keepers (Hotel Trade)</option>
                            <option value='Cleks / Receptionists / Telephone Operators'>Cleks / Receptionists / Telephone Operators</option>
                            <option value='Company Directors'>Company Directors</option>
                            <option value='Computer Personnel'>Computer Personnel</option>
                            <option value='Doctor / Engineer / Lawyer / Accountant / Surveyor'>Doctor / Engineer / Lawyer / Accountant / Surveyor</option>
                            <option value='Drivers / Engine (Locomotive) Drivers'>Drivers / Engine (Locomotive) Drivers</option>
                            <option value='Factory Worker / Sales Girls / Sales Boys'>Factory Worker / Sales Girls / Sales Boys</option>
                            <option value='Farmer'>Farmer</option>
                            <option value='Hair Dresser'>Hair Dresser</option>
                            <option value='Hospital Attendants'>Hospital Attendants</option>
                            <option value='IT Professionals'>IT Professionals</option>
                            <option value='Labor Categories'>Labor Categories</option>
                            <option value='Land owned Planter'>Land owned Planter</option>
                            <option value='Lecturer / Instructor'>Lecturer / Instructor</option>
                            <option value='Machine Operator'>Machine Operator</option>
                            <option value='Managers / Middle Management'>Managers / Middle Management</option>
                            <option value='Marketing Personnel'>Marketing Personnel</option>
                            <option value='Mason / Carpenters / Steel Worker / Welders'>Mason / Carpenters / Steel Worker / Welders</option>
                            <option value='Mechanic / Electrician'>Mechanic / Electrician</option>
                            <option value='Medium size Business Owners'>Medium size Business Owners</option>
                            <option value='Merchant Seaman'>Merchant Seaman</option>
                            <option value='Not Employed'>Not Employed</option>
                            <option value='Nurses'>Nurses</option>
                            <option value='Office Helpers'>Office Helpers</option>
                            <option value='Paramedical services'>Paramedical services</option>
                            <option value='Pilots'>Pilots</option>
                            <option value='Police'>Police</option>
                            <option value='Security Personnel'>Security Personnel</option>
                            <option value='Self Employed'>Self Employed</option>
                            <option value='Senior Managers / All Executives'>Senior Managers / All Executives</option>
                            <option value='Shop Owners'>Shop Owners</option>
                            <option value='Teacher / Principal'>Teacher / Principal</option>
                            <option value='Technical Officers / Foreman'>Technical Officers / Foreman</option>
                        </select>

                        @if ($errors->has('job_category'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('job_category') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Job in Detail</label>
                        <textarea class="form-control{{ $errors->has('job_detail') ? ' is-invalid' : '' }}" rows="5" type="text"  value="{{ old('job_detail') }}" name="job_detail"></textarea>
                        @if ($errors->has('job_detail'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('job_detail') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Job Category</label>
                        <select name="work_place_category" {{old('work_place_category')}} class="form-control">
                            <option value='Corporation or Boards'>Corporation or Boards</option>
                            <option value='Gov'>Gov</option>
                            <option value='Gov or Private Banks'>Gov or Private Banks</option>
                            <option value='Hotel Services'>Hotel Services</option>
                            <option value='Not Working'>Not Working</option>
                            <option value='Other'>Other</option>
                            <option value='Private Sector'>Private Sector</option>
                            <option value='Saloon'>Saloon</option>
                            <option value='Semi Gov'>Semi Gov</option>
                            <option value='Working Overseas(Permernently)'>Working Overseas(Permernently)</option>
                            <option value='Working Overseas(Temporally)'>Working Overseas(Temporally)</option>
                        </select>

                        @if ($errors->has('work_place_category'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('work_place_category') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Work Place in Detail</label>
                        <textarea class="form-control{{ $errors->has('work_place_detail') ? ' is-invalid' : '' }}" rows="5" type="text"  value="{{ old('work_place_detail') }}" name="work_place_detail"></textarea>
                        @if ($errors->has('work_place_detail'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('work_place_detail') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Business in Detail</label>
                        <textarea class="form-control{{ $errors->has('business_detail') ? ' is-invalid' : '' }}" rows="5" type="text"  value="{{ old('business_detail') }}" name="business_detail"></textarea>
                        @if ($errors->has('business_detail'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('business_detail') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Monthly Salary</label>
                        <select name="monthly_salary" {{old('monthly_salary')}} class="form-control">
                            <option value='No Income'>No Income</option>
                            <option value='Below Rs.8,000'>Below Rs.8,000</option>
                            <option value='Rs.8,000 - Rs.15,000'>Rs.8,000 - Rs.15,000</option>
                            <option value='Rs.15,000 - Rs.25,000'>Rs.15,000 - Rs.25,000</option>
                            <option value='Rs.25,000 - Rs.50,000' selected>Rs.25,000 - Rs.50,000</option>
                            <option value='Rs.50,000 - 1 Lak'>Rs.50,000 - 1 Lak</option>
                            <option value='1 - 2 Laks'>1 - 2 Laks</option>
                            <option value='2 - 5 Laks'>2 - 5 Laks</option>
                            <option value='5 - 7 Laks'>5 - 7 Laks</option>
                            <option value='Above 7 Laks'>Above 7 Laks</option>
                        </select>

                        @if ($errors->has('monthly_salary'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('monthly_salary') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Dowry Cash</label>
                        <select name="dowry_cash" {{old('dowry_cash')}} class="form-control" >
                            <option value='Below 50,000'>Below 50,000</option>
                            <option value='50,000-1Lak<'>50,000-1Lak</option>
                            <option value='1Lak-5Laks'>1Lak-5Laks</option>
                            <option value='5Laks-1M'>5Laks-1M</option>
                            <option value='1M-5M'>1M-5M</option>
                            <option value='5M-10M'>5M-10M</option>
                            <option value='above 10M'>above 10M</option>
                            <option value='available but not to mention'>available but not to mention</option>
                            <option value='not available'>not available</option>
                        </select>

                        @if ($errors->has('dowry_cash'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('dowry_cash') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Property Details</label>
                        <select name="property_detail" class="form-control" {{old('property_detail')}} >
                            <option value='Block of land' selected>Block of land</option>
                            <option value='separate house'>separate house</option>
                            <option value='a portion of house'>a portion of house</option>
                            <option value='perental house'>perental house</option>
                            <option value='house and properties'>house and properties</option>
                            <option value='shop space'>shop space</option>
                            <option value='commercial building'>commercial building</option>
                            <option value='A vehicle'>A vehicle</option>
                            <option value='House and vehicle'>House and vehicle</option>
                            <option value='land and vehicle'>land and vehicle</option>
                            <option value='jewelry and furniture only'>jewelry and furniture only </option>
                            <option value='available but not to mention'>available but not to mention</option>
                            <option value='not available'>not available</option>
                        </select>

                        @if ($errors->has('property_detail'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('property_detail') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Lagnaya</label>
                        <select name="lagnaya" class="form-control">
                            <option value='Danu (Sagittarius)'>Danu (Sagittarius)</option>
                            <option value='Kanya (Virgo)'>Kanya (Virgo)</option>
                            <option value='Kataka (Cancer)'>Kataka (Cancer)</option>
                            <option value='Kumba (Aquarius)'>Kumba (Aquarius)</option>
                            <option value='Makara (Capricorn)'>Makara (Capricorn)</option>
                            <option value='Meena (Pisces)'>Meena (Pisces)</option>
                            <option value='Mesha (Aries)'>Mesha (Aries)</option>
                            <option value='Mithuna (Gemini)'>Mithuna (Gemini)</option>
                            <option value='Singhe (Leo)'>Singhe (Leo)</option>
                            <option value='Thula (Libra)'>Thula (Libra)</option>
                            <option value='Vrushaba (Taurus)'>Vrushaba (Taurus)</option>
                            <option value='Wrushika (Scorpio)'>Wrushika (Scorpio)</option>
                        </select>


                        @if ($errors->has('lagnaya'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('lagnaya') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Ganaya</label>
                        <select name="ganaya" {{old('ganaya')}} class="form-control">
                            <option value='Dewa'>Dewa</option>
                            <option value='Manushya'>Manushya</option>
                            <option value='Raksha'>Raksha</option>
                        </select>


                        @if ($errors->has('ganaya'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ganaya') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Nakatha</label>
                        <select name="nekatha" {{old('nekatha')}} class="form-control" >
                            <option value='Ada'>Ada</option>
                            <option value='Anura'>Anura</option>
                            <option value='Aslisa'></option>
                            <option value='Asvida'>Asvida</option>
                            <option value='Berana'>Berana</option>
                            <option value='Deneta'>Deneta</option>
                            <option value='Deta'>Deta</option>
                            <option value='Hatha'>Hatha</option>
                            <option value='Kethi'>Kethi</option>
                            <option value='Maa'>Maa</option>
                            <option value='Moola'>Moola</option>
                            <option value='Muwasirasa'>Muwasirasa</option>
                            <option value='Punawasa'>Punawasa</option>
                            <option value='pusha'>pusha</option>
                            <option value='Puwapal'>Puwapal</option>
                            <option value='Puwaputupa'>Puwaputupa</option>
                            <option value='Puwasala'>Puwasala</option>
                            <option value='Rehena'>Rehena</option>
                            <option value='Revathi'>Revathi</option>
                            <option value='Saa'>Saa</option>
                            <option value='Sitha'>Sitha</option>
                            <option value='Siyawasa'>Siyawasa</option>
                            <option value='Suwana'>Suwana</option>
                            <option value='Uthrapal'>Uthrapal</option>
                            <option value='Uthraputupa'>Uthraputupa</option>
                            <option value='Uthrasala'>Uthrasala</option>
                            <option value='Visa'>Visa</option>
                        </select>


                        @if ($errors->has('ganaya'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ganaya') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Budha</label>
                        <input class="form-control{{ $errors->has('budha') ? ' is-invalid' : '' }}" type="number"  value="{{ old('budha') }}" name="budha">
                        @if ($errors->has('budha'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('budha') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Chandra</label>
                        <input class="form-control{{ $errors->has('chandra') ? ' is-invalid' : '' }}" type="number"  value="{{ old('chandra') }}" name="chandra">
                        @if ($errors->has('chandra'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('chandra') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Guru</label>
                        <input class="form-control{{ $errors->has('guru') ? ' is-invalid' : '' }}" type="number"  value="{{ old('guru') }}" name="guru">
                        @if ($errors->has('guru'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('guru') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Ketu</label>
                        <input class="form-control{{ $errors->has('ketu') ? ' is-invalid' : '' }}" type="number"  value="{{ old('ketu') }}" name="ketu">
                        @if ($errors->has('ketu'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ketu') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Kuja</label>
                        <input class="form-control{{ $errors->has('kuja') ? ' is-invalid' : '' }}" type="number"  value="{{ old('kuja') }}" name="kuja">
                        @if ($errors->has('kuja'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('kuja') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Rahu</label>
                        <input class="form-control{{ $errors->has('rahu') ? ' is-invalid' : '' }}" type="number"  value="{{ old('rahu') }}" name="rahu">
                        @if ($errors->has('rahu'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('rahu') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Ravi</label>
                        <input class="form-control{{ $errors->has('ravi') ? ' is-invalid' : '' }}" type="number"  value="{{ old('ravi') }}" name="ravi">
                        @if ($errors->has('ravi'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ravi') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Shani</label>
                        <input class="form-control{{ $errors->has('shani') ? ' is-invalid' : '' }}" type="number"  value="{{ old('shani') }}" name="shani">
                        @if ($errors->has('shani'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('shani') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Shukra</label>
                        <input class="form-control{{ $errors->has('shukra') ? ' is-invalid' : '' }}" type="number"  value="{{ old('shukra') }}" name="shukra">
                        @if ($errors->has('shukra'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('shukra') }}</strong>
                    </span>
                        @endif
                        <i class="ti-user"></i>
                    </div>
                    <div class="form-group">
                        <label>Diet</label>
                        <select name="diet" {{old('diet')}} class="form-control">
                            <option value='Eggetarian'>Eggetarian</option>
                            <option value='Non Vegetarian'>Non Vegetarian</option>
                            <option value='Vegan'>Vegan</option>
                            <option value='Vegetarian'>Vegetarian</option>
                        </select>


                        @if ($errors->has('diet'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('diet') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Special Cases:(This applies to persons who are physically or ment)</label>
                        <select name="special_case" {{old('special_case')}} class="form-control">
                            <option value="None">None </option>
                            <option value='Mentally challenged (controlled under medication)'>Mentally challenged (controlled under medication)</option>
                            <option value='Mentally challenged (due to accident)'>Mentally challenged (due to accident)</option>
                            <option value='Physically challenged (Not visible)'>Physically challenged (Not visible)</option>
                            <option value='Physically challenged (visible)'>Physically challenged (visible)</option>
                        </select>


                        @if ($errors->has('special_case'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('special_case') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Looking for a Partner from</label>
                        <input class="form-control{{ $errors->has('partner_from') ? ' is-invalid' : '' }}" type="text"  value="{{ old('partner_from') }}" name="partner_from">
                        @if ($errors->has('partner_from'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('partner_from') }}</strong>
                    </span>
                        @endif
                        <i class="ti-location-pin"></i>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5" type="text"  value="{{ old('description') }}" name="description"></textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="profile_image">Profile Photo</label>
                        <input required type="file" class="form-control" name="profile_image" id="profile_image">

                    </div>








                    <div class="form-group">
                        <label>User password</label>
                        <input id="password1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        <i class="icon_lock_alt"></i>
                    </div>
                    <div class="form-group">
                        <label>Confirm password</label>
                        <input id="password2" type="password" class="form-control" name="password_confirmation" required>
                        <i class="icon_lock_alt"></i>
                    </div>
                    <div id="pass-info" class="clearfix"></div>
                    <input type="submit" class="btn_1 rounded full-width add_top_30" value="Create"></input>

                </form>


                <div class="col-md-8 add_top_30">
                    <div class="row">
                        <div class="col-md-6">

    </div>
</div>
<!-- /.container-fluid-->
</div>
<!-- /.container-wrapper-->
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <div class='copy'>© 2019 Hiru Marriage Proposals</div>

        </div>
    </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- Logout Modal-->
<div class="modal fade" id="log_out" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>


</body>


</html>
