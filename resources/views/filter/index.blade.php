
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.ansonika.com/sparker/row-listings-filterscol-search-aside.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 14:11:15 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="SPARKER - Premium directory and listings template by Ansonika.">
    <meta name="author" content="Ansonika">
    <title>Hiru | Member Filter</title>


@include('layouts.theme')

</head>

<body>

<div id="page" class="theia-exception">

    <header class="header_in">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12">
                    <div id="logo">
                        <a href="/">
                            <img src="/img/logo.png" width="165" height="55" alt="" class="logo_sticky">
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-12">
                    <ul id="top_menu">
                        <li><a href="/register" class="btn_add" target="_blank">Register</a></li>
                        @if (Auth::user())
                            <li>
                                <a href="{{ route('login') }}" id="sign-in" class="login" title="Sign In" onclick="event.preventDefault();
                                                 document.getElementById('logout-in').submit();">
                                    Sign In
                                </a>
                                <form id="logout-in" action="{{ route('login') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>

                        @else
                            <li>
                                <a href="#sign-in-dialog" id="sign-in" class="login" title="Sign In">
                                    Sign In
                                </a>
                            </li>
                    @endif
                        <!--<li><a href="#" class="wishlist_bt_top" title="Your wishlist">Your wishlist</a></li>-->
                    </ul>

                    <!-- /top_menu -->
                    <a href="#menu" class="btn_mobile">
                        <div class="hamburger hamburger--spin" id="hamburger">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </a>
                    <nav id="menu" class="main-menu">
                        <ul>
                            <li><span><a href="/">Home</a></span></li>
                            <li><span><a href="/login">My Account</a></span></li>
                            <li><span><a href="/register">Registration</a></span></li>
                            <li><span><a href="#">Application</a></span></li>
                            <li><span><a href="/filter">Members</a></span></li>
                            <li><span><a href='/contact'>Contact Us</a></span></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
        <!-- search_mobile -->
        <div class="layer"></div>
        <div id="search_mobile">
            <a href="#" class="side_panel"><i class="icon_close"></i></a>
            <div class="custom-search-input-2">
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="What are you looking..">
                    <i class="icon_search"></i>
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Where">
                    <i class="icon_pin_alt"></i>
                </div>
                <select class="wide">
                    <option>All Categories</option>
                    <option>Shops</option>
                    <option>Hotels</option>
                    <option>Restaurants</option>
                    <option>Bars</option>
                    <option>Events</option>
                    <option>Fitness</option>
                </select>
                <input type="submit" value="Search">
            </div>
        </div>
        <!-- /search_mobile -->
    </header>
    <!-- /header -->

    <main>
        <div id="results">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-10">
                        <h4>We have <strong>{{$user->count()}}</strong> Members</h4>
                    </div>
                    <div class="col-lg-9 col-md-8 col-2">
                        <a href="#0" class="side_panel btn_search_mobile"></a> <!-- /open search panel -->
                        <div class="row no-gutters custom-search-input-2 inner">

                            <div class="col-lg-5">
                                <form action="/filteroption" method="GET">
                                    @csrf
                                <div class="form-group">
                                    <select class="wide" name="religion" {{old('religion')}}>
                                        <option value='Anglican'>Anglican</option>
                                        <option value='Buddhist' selected>Buddhist</option>
                                        <option value='Catholic'>Catholic</option>
                                        <option value='Christian'>Christian</option>
                                        <option value='Hindu'>Hindu</option>
                                        <option value='Islam'>Islam</option>
                                        <option value='Methodist'>Methodist </option>
                                        <option value='Sevenday'>Sevenday</option>
                                    </select>

                                    <i class="icon-religious-christian"></i>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <select class="wide"  name="gender" {{old('gender')}}>
                                        <option value='Male'>Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    <i class="icon-users"></i>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <select name="country" class="wide" {{old('country')}}>
                                    <option value='Australia'>Australia</option>
                                    <option value='Bahrain'>Bahrain</option>
                                    <option value='Canada'>Canada</option>
                                    <option value='Cyprus'>Cyprus</option>
                                    <option value='Egypt'>Egypt</option>
                                    <option value='Finland'>Finland</option>
                                    <option value='France'>France</option>
                                    <option value='Germany'>Germany</option>
                                    <option value='Hong Kong'>Hong Kong</option>
                                    <option value='India'>India</option>
                                    <option value='Israel'>Israel</option>
                                    <option value='Italy'>Italy</option>
                                    <option value='Japan'>Japan</option>
                                    <option value='Jordan'>Jordan</option>
                                    <option value='Kuwait'>Kuwait</option>
                                    <option value='Malaysia'>Malaysia</option>
                                    <option value='New Zealand'>New Zealand</option>
                                    <option value='Nigeria'>Nigeria</option>
                                    <option value='Norway'>Norway</option>
                                    <option value='Oman'>Oman</option>
                                    <option value='Saudi Arabia'>Saudi Arabia</option>
                                    <option value='Singapore'>Singapore</option>
                                    <option value='South Africa'>South Africa</option>
                                    <option value='Spain'>Spain</option>
                                    <option value='Srilanka' selected>Sri Lanka</option>
                                    <option value='Sweden'>Sweden</option>
                                    <option value='Switzerland'>Switzerland</option>
                                    <option value='United Arab Emirates'>United Arab Emirates</option>
                                    <option value='United Kingdom'>United Kingdom</option>
                                    <option value='United States'>United States</option>
                                    <option value='Zimbabwe'>Zimbabwe</option>
                                    <option value="Other">Other</option>
                                </select>
                                <i class="icon_pin_alt"></i>
                            </div>
                            <div class="col-lg-1">
                                <input type="submit" value="Search">
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /results -->




        <div class="collapse" id="collapseMap">
            <div id="map" class="map"></div>
        </div>
        <!-- /Map -->

        <div class="container margin_60_35">
            <div class="row">

                <!-- /aside -->
@foreach($user as $users)
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="strip grid">
                            <figure>

                                <a href="/home/profile/{{$users['id']}}/view"><img src="/images/profile_images/{{$users->profile_image}}" class="img-fluid" alt=""><div class="read_more"><span>Watch Profile</span></div></a>
                                <small>{{$users->job_category}}</small>
                            </figure>
                            <div class="wrapper">
                                <h3><a href="/home/profile/{{$users['id']}}/view">{{$users->name}} {{$users->last_name}}</a></h3>
                                <small>{{$users->age}} Old</small>
                                <p>{{$users->description}}</p>
                                <a class="address">{{$users->city}}</a>
                            </div>

                        </div>
                    </div>
@endforeach



                </div>
                <!-- /col -->
            </div>
        </div>
        <!-- /container -->

    </main>
    <!--/main-->
@include('layouts.footer')
    <!--/footer-->
</div>
<!-- page -->
@include('layouts.popup')

<div id="toTop"></div><!-- Back to top button -->
<!-- COMMON SCRIPTS -->
<script src="js/common_scripts.js"></script>
<script src="js/functions.js"></script>
<script src="assets/validate.js"></script>


<!-- COLOR SWITCHER  -->
<script src="js/switcher.js"></script>


</body>

<!-- Mirrored from www.ansonika.com/sparker/row-listings-filterscol-search-aside.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 14:11:15 GMT -->
</html>