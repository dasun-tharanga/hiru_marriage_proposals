
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Hiru Marriage Proposals">
    <meta name="author" content="Hiru Marriage Proposals">
    <title>Hiru | Profile</title>


    <!-- YOUR CUSTOM CSS -->

</head>

<body>
        @include('layouts.theme')
<div id="page">


        <!-- /container -->
        @include('layouts.other-navbar')
    <!-- /header -->

    <div class="sub_header_in sticky_header">
        <div class="container">
            <h1>Profile Details</h1>
        </div>
        <!-- /container -->
    </div>
    <!-- /sub_header -->

    <main>

        <!-- /map -->
        <div class="container margin_60_35">
            <div class="row justify-content-center">

                <div class="col-xl-5 col-lg-6 pr-xl-5">
                    <div class="main_title_3">
                        <span></span>
                        <h2>{{$user->name}} {{$user->last_name }}</h2>
                        <p>{{$user->job_category}}</p>
                    </div>
                    <div id="message-contact">

                        <div class="col-xl-5 col-lg-6 pl-xl-5">

                                <img src="/images/profile_images/{{$user->profile_image}}" class="img-responsive" width="auto">

                        </div>

                    </div>

                </div>
                <div class="col-xl-5 col-lg-6 pl-xl-5">
                    <table class="table border-right border-left border-bottom" cellpadding="5" border="0">
                        <tr>
                            <td>Name</td>
                            <td>{{$user->name}} {{$user->last_name }}</td>
                        </tr>

                        <tr>
                            <td>Gender</td>
                            <td>{{$user->gender}}</td>
                        </tr>
                        <tr>
                            <td>Contact No</td>
                            <td>
                                @if(auth()->user())
                                {{$user->mobile_number}}
                                    @else
                                 <button href="#sign-in-dialog" class="btn-sm btn-dark" id="sign-in"><i class="icon-phone"></i>view</button>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                {{$user->email}}
                            </td>

                        </tr>
                        <tr>
                            <td>Marital Status</td>
                            <td>{{$user->marital_status}}</td>
                        </tr>
                        <tr>
                            <td>Country of Living</td>
                            <td>{{$user->country}}</td>
                        </tr>
                        <tr>
                            <td>State Or Province</td>
                            <td>{{$user->province}}</td>
                        </tr>
                        <tr>
                            <td>District or City</td>
                            <td>{{$user->district}}</td>
                        </tr>
                        <tr>
                            <td>City or Village</td>
                            <td>{{$user->city}}</td>
                        </tr>
                        <tr>
                            <td>Residency Status</td>
                            <td>{{$user->recidency_status}}</td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>{{$user->dob}}</td>
                        </tr>
                        <tr>
                            <td>Age</td>
                            <td>{{$user->age}}&nbsp; years</td>
                        </tr>




                        <tr>
                            <td>Height</td>
                            <td>{{$user->height}} cm</td>
                        </tr>

                        <tr>
                            <td>Mother Tongue</td>
                            <td>{{$user->mother_tongue}}</td>
                        </tr>
                        <tr>
                            <td>Religion</td>
                            <td>{{$user->religion}}</td>
                        </tr>
                        <tr>
                            <td>Caste</td>
                            <td>{{$user->caste}}</td>
                        </tr>

                        <tr>
                            <td>Complexion</td>
                            <td>{{$user->complexion}}</td>
                        </tr>
                        <tr>
                            <td>Body type</td>
                            <td>{{$user->body_type}}</td>
                        </tr>
                        <tr>
                            <td>Education Detail</td>
                            <td>{{$user->education_detail}}</td>
                        </tr>
                        <tr>
                            <td> Job in detail</td>
                            <td>{{$user->job_detail}}</td>
                        </tr>
                        <tr>
                            <td>Work place in detail</td>
                            <td>{{$user->work_place_detail}}</td>
                        </tr>
                        <tr>
                            <td> Business in detail </td>
                            <td>{{$user->business_detail}}</td>
                        </tr>
                        <tr>
                            <td>Monthly Salary or Business Income</td>
                            <td>{{$user->monthly_salary}}</td>
                        </tr>
                        <tr>
                            <td>Dowry cash</td>
                            <td>{{$user->dowry_cash}}</td>
                        </tr>
                        <tr>
                            <td>Property detail</td>
                            <td>{{$user->property_detail}}</td>
                        </tr>
                        <tr>
                            <td>Lagnaya</td>
                            <td>{{$user->lagnaya}}</td>
                        </tr>
                        <tr>
                            <td>Nekatha</td>
                            <td>{{$user->nekatha}}</td>
                        </tr>
                        <tr>
                            <td>Ganaya</td>
                            <td>{{$user->ganaya}}</td>
                        </tr>
                        <tr>
                            <td>Ravi(Sun)</td>
                            <td>{{$user->ravi}}</td>
                        </tr>
                        <tr>
                            <td>Chandra (Sandu) (Moon)</td>
                            <td>{{$user->chandra}}</td>
                        </tr>
                        <tr>
                            <td>Kuja (Angaharu) (Mars)</td>
                            <td>{{$user->kuja}}</td>
                        </tr>
                        <tr>
                            <td>Budha (Mercury)</td>
                            <td>{{$user->budha}}</td>
                        </tr>
                        <tr>
                            <td>Guru (Brahaspathi) (Jupiter)</td>
                            <td>{{$user->guru}}</td>
                        </tr>
                        <tr>
                            <td>Shukra (Sikuru) (Venus)</td>
                            <td>{{$user->shukra}}</td>
                        </tr>
                        <tr>
                            <td>Shani (Senasuru) (Saturn)</td>
                            <td>{{$user->shani}}</td>
                        </tr>
                        <tr>
                            <td>Rahu</td>
                            <td>{{$user->rahu}}</td>
                        </tr>
                        <tr>
                            <td>Ketu</td>
                            <td>{{$user->ketu}}</td>
                        </tr>
                        <tr>
                            <td>Diet</td>
                            <td>{{$user->diet}}</td>
                        </tr>
                        <tr>
                            <td>Special Casses</td>
                            <td>{{$user->special_case}}</td>
                        </tr>
                        <tr>
                            <td>Looking From</td>
                            <td>{{$user->partner_from}}</td>
                        </tr>
                        <tr>
                            <td> Self Description</td>
                            <td>{{$user->description}}</td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <!-- /container -->
    </main>
    <!--/main-->

    @include('layouts.footer')
    <!--/footer-->
</div>
<!-- page -->
        @include('layouts.popup')

        <div id="toTop"></div><!-- Back to top button -->

        <!-- COMMON SCRIPTS -->
        <script src="/js/common_scripts.js"></script>
        <script src="/js/functions.js"></script>
        <script src="/assets/validate.js"></script>

        <!-- COLOR SWITCHER  -->
        <script src="/js/switcher.js"></script>
</body>
<!-- Mirrored from www.ansonika.com/sparker/contacts.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jan 2019 14:12:00 GMT -->
</html>
