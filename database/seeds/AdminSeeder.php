<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Hiru',
            'last_name' =>'Admin',
            'mobile_number' => 0456545465,
            'address' => 'sdadsad',
            'province' => 'sad',
            'country'=>'dfsf',
            'city'=>'dfs',
            'email' => 'admin@hirumarriageproposals.lk',
            'email_verified_at' => now(),
            'password' => bcrypt('123456'),
            'admin' => 1,
            'approved_at' => now(),
        ]);
    }
}

