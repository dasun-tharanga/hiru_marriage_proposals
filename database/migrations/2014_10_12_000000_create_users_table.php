<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('mobile_number');
            $table->string('address');
            $table->string('province');
            $table->string('country');
            $table->string('city');
            $table->string('district')->nullable();
            $table->string('recidency_status')->nullable();
            $table->date('dob')->nullable();
            $table->integer('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('mother_tongue')->nullable();
            $table->string('religion')->nullable();
            $table->string('caste')->nullable();
            $table->string('complexion')->nullable();
            $table->string('height')->nullable();
            $table->string('body_type')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('children')->nullable();
            $table->string('education_category')->nullable();
            $table->string('education_detail')->nullable();
            $table->string('job_category')->nullable();
            $table->string('job_detail')->nullable();
            $table->string('work_place_category')->nullable();
            $table->string('work_place_detail')->nullable();
            $table->string('business_detail')->nullable();
            $table->string('monthly_salary')->nullable();
            $table->string('dowry_cash')->nullable();
            $table->string('property_detail')->nullable();
            $table->string('lagnaya')->nullable();
            $table->string('ganaya')->nullable();
            $table->string('nekatha')->nullable();
            $table->integer('budha')->nullable();
            $table->integer('chandra')->nullable();
            $table->integer('guru')->nullable();
            $table->integer('ketu')->nullable();
            $table->integer('kuja')->nullable();
            $table->integer('rahu')->nullable();
            $table->integer('ravi')->nullable();
            $table->integer('shani')->nullable();
            $table->integer('shukra')->nullable();
            $table->string('diet')->nullable();
            $table->string('special_case')->nullable();
            $table->string('partner_from')->nullable();
            $table->string('description')->nullable();
            $table->string('bank_slip')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->timestamp('expired_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
