<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cmgmyr\Messenger\Traits\Messagable;

class User extends Authenticatable
{
    use Notifiable;
    use Messagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'mobile_number',
        'address',
        'province',
        'country',
        'city',
        'recidency_status',
        'district',
        'dob',
        'age',
        'gender',
        'mother_tongue',
        'religion',
        'caste',
        'height',
        'complexion',
        'body_type',
        'marital_status',
        'children',
        'education_category',
        'education_detail',
        'job_category',
        'job_detail',
        'work_place_category',
        'work_place_detail',
        'business_detail',
        'monthly_salary',
        'dowry_cash',
        'property_detail',
        'lagnaya',
        'ganaya',
        'nekatha',
        'budha',
        'chandra',
        'guru',
        'ketu',
        'kuja',
        'rahu' ,
        'ravi',
        'shani',
        'shukra',
        'diet',
        'special_case',
        'partner_from',
        'description',
        'bank_slip',
        'profile_image',
        'email',
        'password',
        'admin',
        'approved_at',
        'expired_at',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $primaryKey = 'id';
}
