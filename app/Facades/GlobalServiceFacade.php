<?php
/**
 * Created by PhpStorm.
 * User: Dasun Tharanga
 * Date: 1/14/2019
 * Time: 12:39 AM
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class GlobalServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Services\GlobalService';
    }
}