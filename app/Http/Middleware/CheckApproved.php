<?php

namespace App\Http\Middleware;
use Carbon\Carbon;
use Closure;
use phpDocumentor\Reflection\Types\Null_;

class CheckApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $now = Carbon::now();
        if (!auth()->user()->approved_at) {
            return redirect()->route('approval')->withMessage('Your account is waiting for our administrator approval.');

        }
        if (auth()->user()->admin != 1) {
            if (auth()->user()->expired_at < $now) {
                auth()->user()->update(
                    [
                    'approved_at' => Null,
                    //'expired_at'=> NULL
                    ]
                );
                return redirect()->route('approval')
                    ->withMessage('Your account has been disabled. Please Contact Us.');

            }
        }








        return $next($request);
    }
}
