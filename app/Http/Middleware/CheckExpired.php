<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class CheckExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $now = Carbon::now();
        if (auth()->user()->admin != 1) {

            if (auth()->user()->expired_at < $now) {
                auth()->user()->update([
                    ['approved_at','=',NULL],
                    ['expired_at','=',NULL],
                ]);
                return redirect()->route('approval')->withMessage('Your account has been disabled.');

            }
        }
        return $next($request);
    }
}
