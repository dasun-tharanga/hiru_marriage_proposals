<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function index(){
        $user = User::where([['approved_at','!=',NULL],['admin','!=',1]])->get();

        return view('filter.index', compact('user'));

    }

}
