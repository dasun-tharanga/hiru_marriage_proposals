<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Hash;




// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{


    public function index()
    {
        $users = User::whereNull('approved_at')->get();

        return view('approval.users', compact('users'));
    }

    public function approve($id)
    {
        $now = Carbon::now();
        $user = User::findOrFail($id);
        $user->update([
            'approved_at' => now(),
            'expired_at' => $now->addMonth(6)->toDateTimeString()
            ]);



        return redirect()->route('admin.users.index')->withMessage('User '.\App\Facades\GlobalServiceFacade::getPaddedId($id).' approved successfully');
    }

    public function view($id){

        $user = User::find($id);
        return view('approval.view',compact('user'));
    }
    public function delete($id){

        User::find($id)->delete();
        return redirect('/home')->withMessage('User '.\App\Facades\GlobalServiceFacade::getPaddedId($id).' Delete Successfully');
    }

    public function add(){
        return view('users.create');
    }


    public function show(){
        return view('users.create');
    }
    public function filter(){
        return view('filter.index');
    }


    public function filteroptin(Request $request){
        $user = User::where([
            ['country','=',$request->country],
            ['religion','=',$request->religion],
            ['gender','=',$request->gender],
            //['age','=',$request->age]
        ])

            ->get();

        //return dd($user);
        return view('filter.index', compact('user'));
    }

    public function filteroptin2(Request $request){
        $user = User::where([
            ['gender','=',$request->gender],
            //['age','=',$request->age]
        ])

            ->get();

        //return dd($user);
        return view('filter.index', compact('user'));
    }





    public function store(\Illuminate\Http\Request $request){
        //$request = Request();
        /*$image = $request->file('bank_slip');
        $image_new = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'),$image_new);
        $img = Image::make(public_path('images/'.$image_new))->resize(700,200);
        $img->save(public_path('images/'.$image_new));*/

        $profile_image = $request->file('profile_image');
        $profile_image_new = rand() . '.' . $profile_image->getClientOriginalExtension();
        $profile_image->move(public_path('images/profile_images'),$profile_image_new);
        $imgs = Image::make(public_path('images/profile_images/'.$profile_image_new))->resize(200,200);
        $imgs->save(public_path('images/profile_images/'.$profile_image_new));




        User::create([
            'name' => $request['name'],
            'last_name' => $request['last_name'],
            'mobile_number' => $request['mobile_number'],
            'address' => $request['address'],
            'country' => $request['country'],
            'province' => $request['province'],
            'email' => $request['email'],
            'city' => $request['city'],
            'district' => $request['district'],
            'dob' => $request['dob'],
            'recidency_status' => $request['recidency_status'],
            'age' => $request['age'],
            'gender' => $request['gender'],
            'mother_tongue' => $request['mother_tongue'],
            'religion' => $request['religion'],
            'caste' => $request['caste'],
            'height' => $request['height'],
            'complexion' => $request['complexion'],
            'body_type' => $request['body_type'],
            'marital_status' => $request['marital_status'],
            'children' => $request['children'],
            'education_category' => $request['education_category'],
            'education_detail' => $request['education_detail'],
            'job_category' => $request['job_category'],
            'job_detail' => $request['job_detail'],
            'work_place_category' => $request['work_place_category'],
            'work_place_detail' => $request['work_place_detail'],
            'business_detail' => $request['business_detail'],
            'monthly_salary' => $request['monthly_salary'],
            'dowry_cash' => $request['dowry_cash'],
            'property_detail' => $request['property_detail'],
            'lagnaya' => $request['lagnaya'],
            'ganaya' => $request['ganaya'],
            'nekatha' => $request['nekatha'],
            'budha' => $request['budha'],
            'chandra' => $request['chandra'],
            'guru' => $request['guru'],
            'ketu' => $request['ketu'],
            'kuja' => $request['kuja'],
            'rahu' => $request['rahu'],
            'ravi' => $request['ravi'],
            'shani' => $request['shani'],
            'shukra' => $request['shukra'],
            'diet' => $request['diet'],
            'special_case' => $request['special_case'],
            'partner_from'=> $request['partner_from'],
            'description' => $request['description'],
           // 'bank_slip' =>$image_new,
            'profile_image' =>$profile_image_new,



            'password' => Hash::make($request['password']),

        ]);

        return redirect('/home/add')->withMessage('User Created Successfully!');

    }











}
