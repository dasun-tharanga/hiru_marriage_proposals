<?php

namespace App\Http\Controllers\Auth;
use App\User;

use App\Http\Controllers\Controller;
use http\Env\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'mobile_number' => ['required','max:10'],
            'address' => ['required'],
            'country' => ['required'],
            'province' => ['required'],
            'city' => ['required'],
            'recidency_status' => ['required'],
            'district' => ['required'],
            'dob'=> ['required'],
            'age' => ['required','integer'],
            'gender' => ['required'],
            'mother_tongue' => ['required'],
            'religion' => ['required'],
           /* 'caste' => ['required'],
            'height' => ['required','integer'],
            'complexion' => ['required'],
            'body_type' => ['required'],
            'marital_status' => ['required'],
            'children' => ['required'],
            'education_category' => ['required'],
            'education_detail' => ['required'],
            'job_category' => ['required'],
            'job_detail' => ['required'],
            'work_place_category' => ['required'],
            'work_place_detail' => ['required'],
            'business_detail' => ['required'],
            'monthly_salary' => ['required'],
            'dowry_cash' => ['required'],
            'property_detail' => ['required'],
            'lagnaya' => ['required'],
            'ganaya' => ['required'],
            'nekatha' => ['required'],
            'budha' => ['required'],
            'chandra' => ['required'],
            'guru' => ['required'],
            'ketu' => ['required'],
            'kuja' => ['required'],
            'rahu' =>['required'],
            'ravi' => ['required'],
            'shani' => ['required'],
            'shukra' => ['required'],
            'diet' => ['required'],
            'special_case' => ['required'],*/
            'partner_from' => ['required'],
            'description' => ['required'],
            'bank_slip'=>['required'],
            'profile_image'=>['required'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function create(array $data)
    {

        $request = Request();
        $image = $request->file('bank_slip');
        $image_new = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'),$image_new);
        $img = Image::make(public_path('images/'.$image_new))->resize(700,200);
        $img->save(public_path('images/'.$image_new));

        $profile_image = $request->file('profile_image');
        $profile_image_new = rand() . '.' . $profile_image->getClientOriginalExtension();
        $profile_image->move(public_path('images/profile_images'),$profile_image_new);
        $imgs = Image::make(public_path('images/profile_images/'.$profile_image_new))->resize(200,200);
        $imgs->save(public_path('images/profile_images/'.$profile_image_new));

        $now = Carbon::now();
        //dd($now->addMonth(6)->toDateTimeString());
        return User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'mobile_number' => $data['mobile_number'],
            'address' => $data['address'],
            'country' => $data['country'],
            'province' => $data['province'],
            'email' => $data['email'],
            'city' => $data['city'],
            'district' => $data['district'],
            'dob' => $data['dob'],
            'recidency_status' => $data['recidency_status'],
            'age' => $data['age'],
            'gender' => $data['gender'],
            'mother_tongue' => $data['mother_tongue'],
            'religion' => $data['religion'],
            'caste' => $data['caste'],
            'height' => $data['height'],
            'complexion' => $data['complexion'],
            'body_type' => $data['body_type'],
            'marital_status' => $data['marital_status'],
            'children' => $data['children'],
            'education_category' => $data['education_category'],
            'education_detail' => $data['education_detail'],
            'job_category' => $data['job_category'],
            'job_detail' => $data['job_detail'],
            'work_place_category' => $data['work_place_category'],
            'work_place_detail' => $data['work_place_detail'],
            'business_detail' => $data['business_detail'],
            'monthly_salary' => $data['monthly_salary'],
            'dowry_cash' => $data['dowry_cash'],
            'property_detail' => $data['property_detail'],
            'lagnaya' => $data['lagnaya'],
            'ganaya' => $data['ganaya'],
            'nekatha' => $data['nekatha'],
            'budha' => $data['budha'],
            'chandra' => $data['chandra'],
            'guru' => $data['guru'],
            'ketu' => $data['ketu'],
            'kuja' => $data['kuja'],
            'rahu' => $data['rahu'],
            'ravi' => $data['ravi'],
            'shani' => $data['shani'],
            'shukra' => $data['shukra'],
            'diet' => $data['diet'],
            'special_case' => $data['special_case'],
            'partner_from'=> $data['partner_from'],
            'description' => $data['description'],
            'bank_slip' =>$image_new,
            'profile_image' =>$profile_image_new,
           // 'expired_at' => $now->addMonth(6)->toDateTimeString(),




            'password' => Hash::make($data['password']),
        ]);
    }
}
