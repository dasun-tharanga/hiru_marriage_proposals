<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()

    {
        if(auth()->user()->admin) {
            $users = User::whereNull('approved_at')->get();

        return view('approval.users', compact('users'));
        }


       else{
        $user = User::find(Auth::user()->id);
        return view('home',compact('user'));
       }


    }

    public function approval()
    {
        return view('approval.approval');
    }



}
