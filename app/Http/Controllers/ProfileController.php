<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class ProfileController extends Controller
{


    public function index(){

        return view('profile.index');
    }


    public function view($id){

        $user = User::find($id);
        return view('profile.index',compact('user'));
    }

    public function edit($id){

        $user = User::find($id);
        return view('profile.edit',compact('user'));
    }

    public function update(Request $request,$id)
    {
        $user = User::find($id);
        $profile_image = $request->file('profile_image');
        $profile_image_new = rand() . '.' . $profile_image->getClientOriginalExtension();
        $profile_image->move(public_path('images/profile_images'), $profile_image_new);
        $imgs = Image::make(public_path('images/profile_images/' . $profile_image_new))->resize(200, 200);
        $imgs->save(public_path('images/profile_images/' . $profile_image_new));



        $user-> name = $request['name'];
        $user-> last_name = $request['last_name'];
        $user-> mobile_number = $request['mobile_number'];
        $user-> address = $request['address'];
        $user-> country = $request['country'];
        $user->province = $request['province'];
        //$user-> email = $request['email'];
        $user-> city = $request['city'];
        $user->district = $request['district'];
        $user->dob = $request['dob'];
        $user->recidency_status = $request['recidency_status'];
        $user->age = $request['age'];
        $user->gender = $request['gender'];
        $user->mother_tongue = $request['mother_tongue'];
        $user-> religion = $request['religion'];
        $user->caste = $request['caste'];
        $user->height = $request['height'];
        $user->complexion = $request['complexion'];
        $user->body_type = $request['body_type'];
        $user->marital_status = $request['marital_status'];
        $user->children = $request['children'];
        $user->education_category = $request['education_category'];
        $user->education_detail = $request['education_detail'];
        $user->job_category = $request['job_category'];
        $user->job_detail = $request['job_detail'];
        $user->work_place_category = $request['work_place_category'];
        $user->work_place_detail = $request['work_place_detail'];
        $user->business_detail = $request['business_detail'];
        $user->monthly_salary = $request['monthly_salary'];
        $user->dowry_cash = $request['dowry_cash'];
        $user->property_detail = $request['property_detail'];
        $user->lagnaya = $request['lagnaya'];
        $user->ganaya = $request['ganaya'];
        $user->nekatha = $request['nekatha'];
        $user->budha = $request['budha'];
        $user->chandra = $request['chandra'];
        $user->guru = $request['guru'];
        $user->ketu = $request['ketu'];
        $user->kuja = $request['kuja'];
        $user->rahu = $request['rahu'];
        $user->ravi = $request['ravi'];
        $user->shani = $request['shani'];
        $user->shukra = $request['shukra'];
        $user->diet = $request['diet'];
        $user->special_case = $request['special_case'];
        $user->partner_from = $request['partner_from'];
        $user->description = $request['description'];
            // 'bank_slip =$image_new,
        $user->profile_image = $profile_image_new;


        $user->password = Hash::make($request['password']);
        $user->update();


        return redirect('/home/')->withMessage('Profile Update Successfully!');

    }


    public function terms()
    {
        return view('terms.index');
    }

}
