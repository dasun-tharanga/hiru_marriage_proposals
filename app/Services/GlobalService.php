<?php

namespace App\Services;

use DateTime;

class GlobalService
{
    /**
     * @param $id
     * @ref     : http://php.net/manual/en/function.str-pad.php
     * @example 00012
     * @return string
     */
    public function getPaddedId($id)
    {
        $padding_length = 9;
        $padding_string = '0';
        $padding_type = STR_PAD_LEFT;

        return str_pad($id, $padding_length, $padding_string, $padding_type);
    }

    /**
     * @param $date
     * @format input format must be Y-m-d H:i:s
     * @return string
     */
    public function formatDBDateToView($date)
    {
        return Datetime::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}