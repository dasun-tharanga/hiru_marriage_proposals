<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $user = \App\User::all()
        ->where('admin','=','0');



//return dd($user->profile_image);
   return view('welcome',compact('user'));
});

Auth::routes();

Route::resource('/profile','ProfileController');
Route::resource('/home/user','UserController');
Route::resource('filter','FilterController');
Route::get('/update','ProfileController@update');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact', 'ContactController@contact');
Route::get('/filter', 'FilterController@index');
Route::get('/filteroption', 'UserController@filteroptin');
Route::get('/filteroption2', 'UserController@filteroptin2');
Route::get('/terms', 'ProfileController@terms');

Route::get('/home/add', 'UserController@show');
Route::get('/home/user/create', 'ProfileController@create');

Route::get('/home/approval/{id}/view','UserController@view')->name('approval-view');
Route::get('/home/approval/{id}/delete','UserController@delete')->name('approval-delete');
Route::get('/home/profile/{id}/view','ProfileController@view')->name('view');
Route::get('/home/profile/{id}/edit','ProfileController@edit')->name('edit');

Route::middleware(['auth'])->group(function () {
    Route::get('/approval', 'HomeController@approval')->name('approval');

    Route::middleware(['approved'])->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
    });

    Route::middleware(['admin'])->group(function () {
        Route::get('/users', 'UserController@index')->name('admin.users.index');
        Route::get('/users/{id}/approve', 'UserController@approve')->name('admin.users.approve');
    });
});

Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});
